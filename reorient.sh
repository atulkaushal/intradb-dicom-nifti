#!/bin/bash

FILENAME=$1
DIRNAME=$(dirname ${FILENAME})
BASENAME=$(basename ${FILENAME} .nii.gz)

# SET UP FSL
export FSLDIR=/nrgpackages/tools.release/fsl-5.0.8-centos6_64
source /nrgpackages/tools.release/fsl-5.0.8-centos6_64/etc/fslconf/fsl.sh
export PATH=${FSLDIR}/bin:${PATH}

xorient=`$FSLDIR/bin/fslval $FILENAME qform_xorient | tr -d ' '`
yorient=`$FSLDIR/bin/fslval $FILENAME qform_yorient | tr -d ' '`
zorient=`$FSLDIR/bin/fslval $FILENAME qform_zorient | tr -d ' '`

echo "Reorienting $FILENAME"
echo "xorient=${xorient}, yorient=${yorient}, zorient=${zorient}"

if [[ "$xorient" != "Right-to-Left" && "$xorient" != "Left-to-Right" || \
      "$yorient" != "Posterior-to-Anterior" || \
      "$zorient" != "Inferior-to-Superior" ]] ; then

  echo "Not LAS or RAS oriented. Running fslreorient2std".
  mv $FILENAME $DIRNAME/${BASENAME}_orig.nii.gz
  $FSLDIR/bin/fslreorient2std $DIRNAME/${BASENAME}_orig.nii.gz $FILENAME

  if [ "$?" -eq "0" ]; then
    rm -f $DIRNAME/${BASENAME}_orig.nii.gz
  fi

else
  echo "Image is LAS or RAS oriented. Skipping fslreorient2std".
fi
