#!/bin/bash
set -x

HOST=${1}
PROJECT=${2}
SUBJECT=${3}
SESSION=${4}
SCAN=${5}
FILENAME=${6}
TOKEN=${7}
SECRET=${8}
BUILD_DIR=${9}
SCRIPT_DIR=${10}
REORIENT=${11}
SPLIT=${12}
JSESSIONID=${13}

#dcm2niix_bin=/nrgpackages/tools.release/dcm2niix-2017.04.28/dcm2niix
dcm2niix_bin=/nrgpackages/tools.release/dcm2niix-2017.10.17/dcm2niix
dcmdump_bin=/nrgpackages/tools/dcmtk/bin/dcmdump

# Set up build space
zip_dir=$BUILD_DIR/zips
dicom_dir=$BUILD_DIR/dicom/$SCAN/$FILENAME
nifti_dir=$BUILD_DIR/nifti/$SCAN
mkdir -p $zip_dir
mkdir -p $dicom_dir
mkdir -p $nifti_dir

# Set up FSL
export FSLDIR=/nrgpackages/tools.release/fsl-5.0.8-centos6_64
source /nrgpackages/tools.release/fsl-5.0.8-centos6_64/etc/fslconf/fsl.sh
export PATH=${FSLDIR}/bin:${PATH}

# Set up Python virtual environment
source /data/intradb/home/hileman/python/venv/bin/activate

scan_uri=$HOST/data/projects/$PROJECT/subjects/$SUBJECT/experiments/$SESSION/scans/$SCAN


main () {
  # Set PCP status to RUNNING
  set_pcp_status
  # Get DICOM resource from XNAT
  download_dicom
  # Check if secondary scan and exit if so
  is_secondary_scan
  # Convert with dcm2niix depending on if setter series or not
  echo "Running dcm2niix v2017.10.17 ..."
  if is_second_setter; then
    dcm2niix_setter
  else
    dcm2niix_standard
  fi
  # Reorient certain series descriptions
  reorient
  # Rename multi-echo files if appropriate
  rename_multi_echo
  # Split requested .nii.gz using fslroi
  split_nifti
  # Upload generated files to XNAT as NIFTI resource
  upload_nifti
}

refresh_jsession () {
  # Check that JSESSIONID from parent proc is still good and get new one if not
  http_code=$(curl -I -s -o /dev/null -k --cookie "JSESSIONID=$JSESSIONID" \
    -w "%{http_code}" $HOST/data/projects)

  if [[ $http_code == "200" ]]; then
    echo $JSESSIONID
  else
    echo $(curl -s -k -u $TOKEN:$SECRET $HOST/data/JSESSIONID)
  fi
}

set_pcp_status () {
  JSESSIONID=$(refresh_jsession)
  uri=$HOST/xapi/pipelineControlPanel/project/$PROJECT/pipeline/DCM2NII/entity/$SESSION/group/ALL/setValues?status=RUNNING
  curl -f -s -k --cookie "JSESSIONID=$JSESSIONID" $uri -X POST -H "Content-Type: application/json"
}

is_second_setter () {
  # Check if 2nd T1w_setter or T2w_setter series
  echo "Checking if this is second T1w or T2w setter."
  dicom_file=$(ls $dicom_dir | head -n 1)
  image_type=$($dcmdump_bin --search ImageType ${dicom_dir}/${dicom_file})
  sequence_name=$($dcmdump_bin --search SequenceName ${dicom_dir}/${dicom_file})

  if [[ $image_type == *"MOSAIC"* && $sequence_name == *"ABCD3d1_32ns"* ]]; then
    echo "2nd setter series found. Converting single frames then combining."
    return 0
  else
    return 1
  fi
}

download_dicom () {
  # Get the DICOM from scan in zip archive and extract
  echo "Getting DICOM resource zip"
  dicom_zip=${SESSION}_scan${SCAN}.zip
  uri=$scan_uri/resources/DICOM/files?format=zip
  JSESSIONID=$(refresh_jsession)
  curl -f -s -k --cookie "JSESSIONID=$JSESSIONID" $uri > $zip_dir/$dicom_zip
  unzip -o -j $zip_dir/$dicom_zip -d $dicom_dir
}

is_secondary_scan () {
  # Check dicom to see if this is a secondary scan and exit if so
  # source /nrgpackages/scripts/epd-python_setup.sh
  python $SCRIPT_DIR/secondary-capture.py $dicom_dir
  if [ "$?" -eq "1" ]; then
    echo "$SCAN is a secondary capture"
    exit 0
  fi
}

dcm2niix_standard () {
  $dcm2niix_bin -z y -o $nifti_dir -b y -ba y -d n -f %f $dicom_dir
}

dcm2niix_setter () {
  # Convert each frame individually and combine with fslmerge
  #for dicom in $(find $dicom_dir -name "*dcm"); do
  # Some dicoms coming in without .dcm extension
  for dicom in $(find $dicom_dir -type f); do
    acq_num=$($dcmdump_bin --search AcquisitionNumber $dicom | \
      cut -d"[" -f 2 | cut -d"]" -f 1 | xargs -I '{}' printf "%03g" '{}')
    $dcm2niix_bin -z y -o $nifti_dir -b y -ba y -d n -s y -f %f_a${acq_num} $dicom
  done

  # Merge into single NIFTI file
  fslmerge -t $nifti_dir/${FILENAME} $nifti_dir/${FILENAME}_a???.nii.gz

  # Remove all the single frame nifti files
  rm -f $nifti_dir/${FILENAME}_a*.nii.gz

  # Combine all individual json files
  python $SCRIPT_DIR/combine-json.py $nifti_dir $FILENAME

  # Remove all the single json files
  rm -f $nifti_dir/${FILENAME}_a*.json
}

rename_multi_echo () {
  # Check if multi-echo and add _e1 suffix to the first file
  if [[ $(ls ${nifti_dir}/*_e2.nii.gz) ]]; then
    echo "Renaming vNav files .."
    mv $nifti_dir/${FILENAME}.nii.gz $nifti_dir/${FILENAME}_e1.nii.gz
    mv $nifti_dir/${FILENAME}.json $nifti_dir/${FILENAME}_e1.json
  fi
}

reorient () {
  # See if it needs reoriented using fslreorient2std
  if [ $REORIENT = "true" ]; then
    echo "NOT adjusting the value for PhaseEncodingDirection in the original BIDS sidecar (.json) file."
    echo "Check whether PhaseEncodingDirection is still accurate after reorienting, and correct if necessary."

    for nifti in $(find $nifti_dir -name "*.nii.gz"); do
      echo "Reorienting $nifti"
      $SCRIPT_DIR/reorient.sh $nifti
    done
  fi
}

split_nifti () {
  if [[ $SPLIT -lt 1 ]]; then
    return
  fi

  mv $nifti_dir/$FILENAME.nii.gz $nifti_dir/${FILENAME}_orig.nii.gz
  fslroi $nifti_dir/${FILENAME}_orig.nii.gz $nifti_dir/${FILENAME}_InitialFrames.nii.gz 0 $SPLIT
  fslroi $nifti_dir/${FILENAME}_orig.nii.gz $nifti_dir/${FILENAME}.nii.gz $SPLIT -1

  if [ "$?" -eq "0" ]; then
    rm -f $nifti_dir/${FILENAME}_orig.nii.gz
  else
    exit 2
  fi
}

upload_nifti () {
  # Create NIFTI resource for scan and POST files to resource
  echo "Creating NIFTI resource and uploading files"
  uri=$scan_uri/resources/NIFTI
  JSESSIONID=$(refresh_jsession)
  curl -f -s -k --cookie "JSESSIONID=$JSESSIONID" -X PUT $uri?format=NIFTI

  for filename in $nifti_dir/*; do
    curl -f -s -k --cookie "JSESSIONID=$JSESSIONID" -X PUT \
      $uri/files/$(basename $filename) -F "$filename=@$filename"
  done
}

main
